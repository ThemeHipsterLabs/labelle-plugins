<?php
/*
Plugin Name: HipsterLab
Plugin URI: http://hipsterlab.com/
Description: HipsterLab eCommerce Core
Version: 1.0.0
Author: HipsterLab
Author URI: http://hipsterlab.com/
License: GPLv2 or later
Text Domain: labelle
*/

// Register Custom Post Type
function labelle_post_type_static_block() {

	$labels = array(
		'name'                  => _x( 'Static Blocks', 'Post Type General Name', 'labelle' ),
		'singular_name'         => _x( 'Static Block', 'Post Type Singular Name', 'labelle' ),
		'menu_name'             => __( 'Static Blocks', 'labelle' ),
		'name_admin_bar'        => __( 'Static Block', 'labelle' ),
		'archives'              => __( 'Static Block Archives', 'labelle' ),
		'parent_item_colon'     => __( 'Parent Item:', 'labelle' ),
		'all_items'             => __( 'All Items', 'labelle' ),
		'add_new_item'          => __( 'Add New Item', 'labelle' ),
		'add_new'               => __( 'Add New', 'labelle' ),
		'new_item'              => __( 'New Item', 'labelle' ),
		'edit_item'             => __( 'Edit Item', 'labelle' ),
		'update_item'           => __( 'Update Item', 'labelle' ),
		'view_item'             => __( 'View Item', 'labelle' ),
		'search_items'          => __( 'Search Item', 'labelle' ),
		'not_found'             => __( 'Not found', 'labelle' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'labelle' ),
		'featured_image'        => __( 'Featured Image', 'labelle' ),
		'set_featured_image'    => __( 'Set featured image', 'labelle' ),
		'remove_featured_image' => __( 'Remove featured image', 'labelle' ),
		'use_featured_image'    => __( 'Use as featured image', 'labelle' ),
		'insert_into_item'      => __( 'Insert into item', 'labelle' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'labelle' ),
		'items_list'            => __( 'Items list', 'labelle' ),
		'items_list_navigation' => __( 'Items list navigation', 'labelle' ),
		'filter_items_list'     => __( 'Filter items list', 'labelle' ),
	);
	$args = array(
		'label'                 => __( 'Static Block', 'labelle' ),
		'description'           => __( 'Static Block', 'labelle' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-editor-kitchensink',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'static_block', $args );

}
add_action( 'init', 'labelle_post_type_static_block', 0 );
// Widget
require_once dirname( __FILE__ ) . '/widget/recent-post.php';
