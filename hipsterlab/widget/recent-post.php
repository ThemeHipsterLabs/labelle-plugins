<?php
/**
 * Recent Post Widget
 */
function hipster_latest_recent_load_widget() {
	register_widget( 'Hipster_latest_recent_widget' );
}
add_action( 'widgets_init', 'hipster_latest_recent_load_widget' );
class Hipster_latest_recent_widget extends WP_Widget {

	function hipster_latest_recent_widget() {
		$widget = array(
			'classname' => 'hipster_latest_recent_widget',
			'description' => esc_html__('Displays your recent posts', 'hipster_latest_recent_widget')
		);

		$control = array(
			'width' => 250,
			'height' => 300,
			'id_base' => 'hipster_latest_recent_widget'
		);
		parent::__construct( 'hipster_latest_recent_widget', esc_html__('Hipster Recent Posts', 'hipster_latest_recent_widget'), $widget, $control );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );
		$categories = $instance['categories'];
		$number = $instance['number'];

		$query = array(
			'posts_per_page' => $number,
			'ignore_sticky_posts' => 1,
			'post_status' => 'publish',
			'cat' => $categories
		);

		$recent_posts = new WP_Query($query);

		if ($recent_posts->have_posts()) :

		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;

		?>
			<div class="wrap-recent-posts">
			<?php while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
				<div class="sidebar-post-item">
					<div class="sidebar-post-thumb">
						<a href="<?php echo get_permalink() ?>">
						<?php if ( has_post_thumbnail() ): ?>
							<?php the_post_thumbnail( 'thumb-450-300' ); ?>
						<?php else: ?>
							<img src="http://placehold.it/450x300" alt="thumbnail"/>
						<?php endif; ?>
						</a>
					</div>
					<div class="sidebar-post-meta">
						<p><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a><p>
						<span><?php the_time( get_option('date_format') ); ?></span>
					</div>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
			</div>
		<?php endif; ?>

		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['categories'] = $new_instance['categories'];
		$instance['number'] = strip_tags( $new_instance['number'] );
		return $instance;
	}


	function form( $instance ) {
		$defaults = array(
			'title' => esc_html__('Recent Posts', 'hipster'),
			'number' => 5,
			'categories' => ''
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'hipster'); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>"  />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('categories'); ?>"><?php esc_html_e('Choose category:','hipster');?></label>
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>><?php esc_html_e(' All categories','hipster' );?></option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category):?>
					<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php endforeach;?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e('Number of posts:', 'hipster'); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" size="3" />
		</p>


	<?php
	}
}